import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {CountriesView} from './components/CountriesView';
import {QueryClient, QueryClientProvider} from 'react-query';
import { HolidaysView } from './components/HolidaysView';
import { Fallback } from './components/Fallback';

enum ROUTES {
  MAIN = '/',
  COUNTRY_HOLIDAYS = '/holidays/:countryCode',
}

export const App: React.FC = () => {
  const queryClient = new QueryClient({defaultOptions: {queries: {refetchOnWindowFocus: false}}});

  return(
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <Routes>
          <Route path={ROUTES.COUNTRY_HOLIDAYS} element={<HolidaysView/>} />
          <Route path={ROUTES.MAIN} element={<CountriesView/>} />
          <Route path="*" element={<Fallback />} />
        </Routes>
      </BrowserRouter>
    </QueryClientProvider>
  );
};
