import {axios} from './axios';
import { CountryList, HolidayList } from '../types/Api';

export const Api = {
  getCountries: function(search?: string, country?: string) {
    return axios.get<CountryList>('/countries', {params: {search, country}}).then(response => response.data);
  },

  getHolidaysForCountry: function(country: string, publicOnly: boolean) {
    return axios.get<HolidayList>('/holidays', {params: {country, year: 2020, public: publicOnly}}).then(response => response.data);
  }
};
