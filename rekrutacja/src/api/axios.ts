import Axios from "axios";

export const key = process.env.REACT_APP_API_KEY; //Plik .env powinien być w gitignore jeśli są tam takie dane jak klucz api, ale na potrzeby tego zadania nie umieściłem go tam
const baseURL = 'https://holidayapi.com/v1';

export const axios = Axios.create({baseURL});
axios.interceptors.request.use((request) => {
  const oldParams = request.params;
  request.params = {key, ...oldParams};
  return request;
});