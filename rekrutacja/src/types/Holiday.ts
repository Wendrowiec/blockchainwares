type Weekday = {
  name: string;
  numeric: number;
};

type HolidayWeekday = {
  date: Weekday;
  observed: Weekday;
};

export type Holiday = {
  country: string;
  date: string;
  name: string;
  observed: string;
  public: boolean;
  uuid: string;
  weekday: HolidayWeekday
};
