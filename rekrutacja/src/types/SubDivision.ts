export type SubDivision = {
  code: string;
  name: string;
  languages: string[];
}