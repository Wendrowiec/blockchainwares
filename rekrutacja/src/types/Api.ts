import { Country } from "./Country";
import {Holiday} from './Holiday';

type RequestsObject = {
  used: number;
  available: number;
  resets: Date;
};

type List = {
  status: number;
  requests: RequestsObject;
};

export type CountryList = List & {
  countries: Country[];
};

export type HolidayList = List & {
  holidays: Holiday[];
};
