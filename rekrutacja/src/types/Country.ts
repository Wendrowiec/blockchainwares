import { SubDivision } from "./SubDivision";

export type Country = SubDivision & {
  codes: {};
  flag: string;
  subdivisions: SubDivision[];
}