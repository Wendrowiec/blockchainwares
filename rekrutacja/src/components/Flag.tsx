import React from 'react';
import { CircularProgress } from '@mui/material';
import { LazyLoadImage } from 'react-lazy-load-image-component';

interface FlagProps {
  countryCode: string;
}

export const Flag: React.FC<FlagProps> = ({countryCode}) => {
  const [loaded, setLoaded] = React.useState<boolean>(false);
  
  return(
    <>
      {!loaded && <CircularProgress/>}
      <LazyLoadImage src={`https://flagcdn.com/64x48/${countryCode.toLowerCase()}.png`} alt="FlagImageError" beforeLoad={setLoaded.bind(null , true)} /> {/* countryflags.io nie zyje, więc uzycie country.flag jest niemozliwe */}
    </>
  );
};
