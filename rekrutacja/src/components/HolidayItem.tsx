import React from 'react';
import {Holiday} from '../types/Holiday';
import {Grid, Typography} from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

interface HolidayItemProps {
  holiday: Holiday;
};

export const HolidayItem: React.FC<HolidayItemProps> = ({holiday}) => {
  return (
    <Grid container justifyContent='space-between' alignItems='center' style={holiday.public? {color: 'rgb(25, 118, 210)'}: undefined}>
      <Grid item>
        <Typography>{holiday.date}</Typography>
      </Grid>
      <Grid item>
        <Typography>{holiday.name}</Typography>
      </Grid>
      <Grid item>
        {holiday.public ? < CheckIcon />: <CloseIcon/>}
      </Grid>
    </Grid>
  );
};
