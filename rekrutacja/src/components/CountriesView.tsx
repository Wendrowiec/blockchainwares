import React from 'react';
import { Api } from '../api/Api';
import {Grid, Box, Typography, CircularProgress, TextField} from '@mui/material';
import { CountryItem } from './CountryItem';
import {useQuery} from 'react-query';

export const CountriesView: React.FC = () => {
  const [searchInput, setSearchInput] = React.useState<string>('');

  const {data} = useQuery(['countries', searchInput.length > 1 ? searchInput: undefined], () => Api.getCountries(searchInput.length > 1 ? searchInput: undefined));

  const handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchInput(event.target.value);
  };

  return(
    <Box style={{width: '100vw', height: '100vh'}}>
      <Grid container direction='column' justifyContent='center' alignItems='center' style={{padding: '2rem'}}>
        <Grid item>
          <Typography variant="h1">Countries</Typography>
        </Grid>
        <Grid item>
          <TextField label='Search' value={searchInput} onChange={handleSearchInputChange} variant='filled' />
        </Grid>
        <Grid item style={{marginTop: '1rem', width: '100%'}}>
          <Grid container justifyContent='center' alignItems='center'>
            {data?.countries === undefined ? <CircularProgress/>: data.countries.map((country) => <Grid item key={country.code} xs={2} style={{marginRight: '1rem', marginBottom: '1rem'}}><CountryItem country={country} /></Grid>)}
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}
