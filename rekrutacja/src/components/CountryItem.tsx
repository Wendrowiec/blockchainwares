import React from 'react';
import { Country } from '../types/Country';
import {Paper, Grid, Typography} from '@mui/material';
import {Link} from 'react-router-dom';
import {Flag} from './Flag';

interface CountryItemProps {
  country: Country;
}

export const CountryItem: React.FC<CountryItemProps> = ({country}) => {
  return(
    <Paper elevation={1} style={{height: '11rem'}}>
      <Link to={`/holidays/${country.code}`} style={{color: '#000', textDecoration: 'none'}}>
        <Grid container direction='column' justifyContent='center' alignItems='center' style={{padding: '1rem'}}>
          <Grid item>
            <Flag countryCode={country.code} />
          </Grid>
          <Grid item style={{marginTop: '1rem'}}>
            <Typography>{country.name}</Typography>
          </Grid>
          <Grid item>
            <Typography>{country.code}</Typography>
          </Grid>
        </Grid>
      </Link>
    </Paper>
  );
};
