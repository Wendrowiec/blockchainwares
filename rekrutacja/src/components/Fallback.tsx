import React from 'react';
import {Box, Grid, Typography, Button} from '@mui/material';
import {Link} from 'react-router-dom';

export const Fallback: React.FC = () => {
  return(
    <Box style={{width: '100vw', height: '100vh'}}>
      <Grid container direction='column' justifyContent='center' alignItems='center' style={{height: '100%'}}>
        <Grid item>
          <Typography variant='h2'>The page you are looking for does not exist ;(</Typography>
        </Grid>
        <Grid item>
          <Link to='/' >
            <Button variant='contained'>Home Page</Button>
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
};
