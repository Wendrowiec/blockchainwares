import React from 'react';
import { useParams } from 'react-router';
import { useQuery } from 'react-query';
import {Grid, Typography, CircularProgress, Checkbox, FormControlLabel} from '@mui/material';
import { Api } from '../api/Api';
import {Flag} from './Flag';
import { HolidayItem } from './HolidayItem';

export const HolidaysView: React.FC = () => {
  const {countryCode} = useParams();

  const [publicOnly, setPublicOnly] = React.useState<boolean>(false);

  const {data: countryData} = useQuery(['country', countryCode], () => Api.getCountries(undefined, countryCode));
  const {data} = useQuery(['holidays', countryCode, publicOnly], () => Api.getHolidaysForCountry(countryCode || '', publicOnly));

  const handlePubllicOnlyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPublicOnly(event.target.checked);
  };

  return(
    <Grid container direction='column' justifyContent='center' alignItems='center' style={{width: '100%', padding: '0 2rem'}}>
      <Grid item>
        <Typography variant='h1'>Holidays</Typography>
      </Grid>
      <Grid item>
        {countryData === undefined ? <CircularProgress/>: 
          <Grid container justifyContent='center' alignItems='center'>
            <Grid item style={{marginRight: '2rem'}}>
              {countryData.countries[0].name}
            </Grid>
            <Grid item style={{marginRight: '2rem'}}>
              <Flag countryCode={countryCode || ''} />
            </Grid>
            <Grid item>
              <FormControlLabel control={<Checkbox value={publicOnly} onChange={handlePubllicOnlyChange} />} label='Show only public holidays' />
            </Grid>
          </Grid>
        }
      </Grid>
      <Grid item style={{width: '100%', marginTop: '1.5rem'}}>
        {data?.holidays === undefined ? <CircularProgress />: 
          <Grid container direction='column' justifyContent='center' alignItems='center' style={{width: '100%'}}>
            <Grid item style={{width: '100%'}}>
              <Grid container justifyContent='space-between' alignItems='center'>
                <Grid item>
                  <Typography>Date</Typography>
                </Grid>
                <Grid item>
                  <Typography>Name</Typography>
                </Grid>
                <Grid item>
                  <Typography>Public</Typography>
                </Grid>
              </Grid>
            </Grid>
            {data.holidays.map((holiday) => <Grid item key={holiday.uuid} style={{width: '100%'}}><HolidayItem holiday={holiday} /></Grid>)}
          </Grid>
        }
      </Grid>
    </Grid>
  );
};
